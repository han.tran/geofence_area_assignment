//
//  Blockable.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/29/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//

import UIKit

protocol Blockable {}
extension Blockable {
    func using(block: (Self) -> Void) {
        block(self)
    }

    func apply(block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
}

extension Bool: Blockable { }
extension CGFloat: Blockable { }
extension NSObject: Blockable { }
