//
//  NetworkListner.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/31/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//

import Foundation
class NetworkListner : NSObject {
    static  let shared = NetworkListner()
    var reachabilityStatus: Reachability.Connection = .unavailable
    let reachability = try! Reachability()
    var sendStatus: ((Reachability.Connection) -> ())? = nil
    var isNetworkAvailable : Bool {
        return reachabilityStatus != .unavailable
    }
    
    
    func startNWListner() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
            self.sendStatus?(.wifi)
        case .cellular:
            print("Reachable via Cellular")
            self.sendStatus?(.cellular)
        case .unavailable:
            self.sendStatus?(.unavailable)
            print("Network not reachable")
        case .none:
            self.sendStatus?(.none)
            print("Network none")
        }
    }
    
    
}
