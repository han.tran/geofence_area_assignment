//
//  UITableViewExtension.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/29/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//

import Rswift
import UIKit

extension UITableView {
    func register(header type: NibResourceType) {
        self.register(UINib(resource: type), forHeaderFooterViewReuseIdentifier: type.name)
    }
    func register(cell type: NibResourceType) {
        self.register(UINib(resource: type), forCellReuseIdentifier: type.name)
    }
    func register(cell: AnyClass) {
        self.register(cell, forCellReuseIdentifier: String(describing: cell))
    }
    func register(header: AnyClass) {
        self.register(header, forHeaderFooterViewReuseIdentifier: String(describing: header))
    }
    func cell<T>(_ type: NibResourceType, for indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withIdentifier: type.name, for: indexPath) as? T
    }
    func cell<T>(for indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withIdentifier: "\(T.self)", for: indexPath) as? T
    }
    func view<T>(_ type: NibResourceType) -> T? {
        return self.dequeueReusableHeaderFooterView(withIdentifier: type.name) as? T
    }
    func view<T>() -> T? {
        return self.dequeueReusableHeaderFooterView(withIdentifier: "\(T.self)") as? T
    }
}
