//
//  CoordinatesViewController.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/29/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//

import UIKit
import RealmSwift

class CoordinatesViewController: UIViewController {
    
    @IBOutlet weak var wifi: UITextField!
    @IBOutlet weak var latTxtFld: UITextField!
    @IBOutlet weak var longTxtFld: UITextField!
    var coordinateId = ""
    fileprivate var coordinatesObject: CoordinatesObject!
    let realm = try! Realm()
    
    override func viewDidLoad() {
        
        self.title = "Update Coordinates"
        navigationController?.navigationBar.barTintColor = .white
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        coordinatesObject = realm.objects(CoordinatesObject.self).filter("id = '\(coordinateId)'").first
        let wifiObject = realm.objects(WifiObject.self).filter("regionId = '\(coordinatesObject.regionId)'").first

        latTxtFld.text = "\(coordinatesObject.latitude)"
        longTxtFld.text = "\(coordinatesObject.longitude)"
        wifi.text =  "\(wifiObject?.name ?? "")"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func save(_ sender: Any) {
        
        coordinatesObject = realm.objects(CoordinatesObject.self).filter("id = '\(coordinateId)'").first
        let wifiObject = realm.objects(WifiObject.self).filter("regionId = '\(coordinatesObject.regionId)'").first

        try! self.realm.write {
            coordinatesObject.latitude = Double(latTxtFld.text!)!
            coordinatesObject.longitude = Double(longTxtFld.text!)!
            wifiObject?.name = wifi.text!
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
