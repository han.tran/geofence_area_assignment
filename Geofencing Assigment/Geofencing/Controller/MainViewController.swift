//
//  MainViewController.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/29/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import UserNotifications
import RealmSwift
import Toaster

class MainViewController: UIViewController {
    @IBOutlet weak var map: MKMapView!
    let locationManager = CLLocationManager()
    var longGesture = UILongPressGestureRecognizer()
    fileprivate var status:String!
    
    let realm = try! Realm()
    
    fileprivate var regionObjects:Results<RegionObject>!
    fileprivate var coordinatesObjects:  Results<CoordinatesObject>!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        polyCheck = false
        circleCheck = false
        regionObjects = realm.objects(RegionObject.self)
        self.map.removeOverlays(self.map.overlays)
        self.map.removeAnnotations(self.map.annotations)
        map.showsUserLocation = true
        regionObjects.forEach({
            coordinatesObjects = realm.objects(CoordinatesObject.self).filter("regionId = '\($0.id)'")
            drawOverlays(coordinatesObject: coordinatesObjects, regionObject: $0)
        })
        self.locate(self.map!)
        NetworkListner.shared.sendStatus  = {[unowned self]  status in
            self.locate(self.map!)
            switch status {
            case .wifi:
                print("Reachable via WiFi")
                Toast(text: "Reachable via WiFi").show()
            case .cellular:
                print("Reachable via Cellular")
                Toast(text: "Reachable via Cellular").show()
            case .unavailable:
                print("Network not reachable")
                Toast(text: "Network not reachable").show()
                self.title = "Monitoring"
                self.navigationController?.navigationBar.barTintColor = .white
            case .none:
                print("Network none")
                Toast(text: "Network none").show()
                self.title = "Monitoring"
                self.navigationController?.navigationBar.barTintColor = .white
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.locationManager.stopUpdatingLocation()
    }
    func drawOverlays(coordinatesObject:Results<CoordinatesObject> , regionObject:RegionObject) {
        
        if regionObject.type == "circle" {
            if coordinatesObject.count > 0 {
                let location = CLLocationCoordinate2D(latitude: coordinatesObject[0].latitude, longitude: coordinatesObject[0].longitude)
                // startMonitoring
                locationManager.startMonitoring(for: region(with: location,radius: regionObject.radius))
                ///
                let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
                map.setRegion(region, animated: true)
                // add annotation
                let annotation = MKPointAnnotation()
                annotation.coordinate = location
                annotation.title = regionObject.title
                map.addAnnotation(annotation)
                // show overlay
                let circle = MKCircle(center: location,
                                      radius: regionObject.radius)
                map.addOverlay(circle)
            }
            locationManager.startUpdatingLocation()
        }
        
        if regionObject.type == "polygon" {
            
            if coordinatesObject.count > 0 {
                
                // self.map.removeOverlays(self.map.overlays)
                
                // self.map.removeAnnotations(self.map.annotations)
                
                let location = CLLocationCoordinate2D(latitude: coordinatesObject[0].latitude, longitude: coordinatesObject[0].longitude)
                
                let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
                map.setRegion(region, animated: true)
                // add annotation
                let annotation = MKPointAnnotation()
                annotation.coordinate = location
                annotation.title = regionObject.title
                map.addAnnotation(annotation)
                // show overlay
                
                var polygonCoordinate = [CLLocationCoordinate2D]()
                
                coordinatesObject.forEach({
                    polygonCoordinate.append(CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude))
                })
                self.polygonCoordinate = polygonCoordinate
                
                let polygon = MKPolygon(coordinates: polygonCoordinate, count: polygonCoordinate.count)
                map.addOverlay(polygon)
            }
            locationManager.startUpdatingLocation()

        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Monitering"
        hideKeyboardWhenTappedAround()
        // notification setup
        if notificationSetup() {
            print("Notification Setup")
        } else {
            print("Notification Not Setup")
        }
        /// map delegate
        map.delegate = self
        // locationManager
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        locationManager.startUpdatingLocation()
        locationManager.distanceFilter = 1
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.allowsBackgroundLocationUpdates = true
        
        
        // stop any existing monitered region
        stopExistingMoniteredRegion()
        
        // add Gesture to get touch Location on map
        
        longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        longGesture.minimumPressDuration = 0.5
        map.addGestureRecognizer(longGesture)
        
        
        //  self.setupCircleGeofencing(title: "test", lat: 3.116103930822632 , long: 101.63855281568493, radius: 100)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func notificationSetup () -> Bool {
        var notifCheck = true
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) {
            (granted, error) in
            if granted {
                notifCheck =  true
            } else {
                notifCheck =  false
            }
        }
        
        return notifCheck
    }
    
    func stopExistingMoniteredRegion()  {
        // stop any existing monitered region
        for itemRegion in locationManager.monitoredRegions {
            locationManager.stopMonitoring(for: itemRegion)
        }
    }
    
    func scheduleNotification(title: String, body: String) {
        
        let content = UNMutableNotificationContent()
        
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        content.badge = 1
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false) // send notification in 0.1 sec
        let request = UNNotificationRequest(identifier: randomString(length: 10), content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    @objc func longPress(_ sender: UILongPressGestureRecognizer) {
        if sender.state != UIGestureRecognizer.State.began { return }
        let touchLocation = sender.location(in: map)
        let locationCoordinate = map.convert(touchLocation, toCoordinateFrom: map)
        print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
        
//        chooseAlert(withTitle: "IMPORT", message: "PLEASE CHOOSE GEOFENCING TYPE", lat:locationCoordinate.latitude ,long: locationCoordinate.longitude)
        self.circleAlert(withTitle: "CIRCLE", message: "PLEASE ENTER REGION TITLE AND RADIUS \n lat : \(locationCoordinate.latitude) \n long:\(locationCoordinate.longitude)", lat: locationCoordinate.latitude, long: locationCoordinate.longitude)
        
        print("")
    }
    
    func circleAlert (withTitle title: String?, message: String?, lat : Double , long : Double) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "Input region title .."
        })
        
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "Input region radius in metter.."
        })
        
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "Input Wifi Name  .."
        })
        
//        alert.addTextField(configurationHandler: { textField in
//            textField.placeholder = "Input Wifi Mac Address  .."
//        })
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            //
            //            if let title = alert.textFields?[0].text {
            //                print("title: \(title)")
            //            }
            
            let titleAlert = alert.textFields?[0].text
            let radiusAlert = alert.textFields?[1].text
            let regionId = self.randomString(length: 10)
            let ssidAlert = alert.textFields?[2].text
//            let bssidAlert = alert.textFields?[3].text
            
            let coordinate = CoordinatesObject()
            coordinate.id = self.randomString(length: 10)
            coordinate.regionId = regionId
            coordinate.latitude = lat
            coordinate.longitude = long
            
            try! self.realm.write {
                self.realm.add(coordinate)
            }
            let regionObject = RegionObject()
            regionObject.id = regionId
            regionObject.title = titleAlert!
            regionObject.type = "circle"
            regionObject.radius = Double(radiusAlert!)!
            
            try! self.realm.write {
                self.realm.add(regionObject)
            }
            
            let wifiObject = WifiObject()
            wifiObject.id = self.randomString(length: 10)
            wifiObject.regionId = regionId
            wifiObject.name = ssidAlert!
            wifiObject.macAddress = "macAddress"
            
            try! self.realm.write {
                self.realm.add(wifiObject)
            }
            
            self.setupCircleGeofencing(title: titleAlert!, lat: lat, long: long, radius: Double(radiusAlert!)!,identifier:  regionId)
            self.locate(self.map!)
        }))
        
        self.present(alert, animated: true)
    }
    
    var regionId:String! = "initial"
    
    func chooseAlert(withTitle title: String?, message: String?, lat : Double , long : Double) {
        
        
        
        if polygonCoordinate.count >  0 && !polyCheck { // must be 2 coords or more
            self.polygonAlert(withTitle: "POLYGON", message: "ADDING TO POLYGON SHAPE \n lat : \(lat) \n long:\(long)", lat: lat, long: long)
        } else {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let circle = UIAlertAction(title: "CIRCLE", style: .default, handler: { action in
                self.polygonCoordinate.removeAll() // clean path
                self.circleAlert(withTitle: "CIRCLE", message: "PLEASE ENTER REGION TITLE AND RADIUS \n lat : \(lat) \n long:\(long)", lat: lat, long: long)
            })
            let polygon = UIAlertAction(title: "POLYGON", style: .default, handler: { action in
                self.polygonCoordinate.removeAll() // clean path
                self.stopExistingMoniteredRegion() //  stop other monitored regoins
                self.polygonAlert(withTitle: "POLYGON", message: "ADDING TO POLYGON SHAPE \n lat : \(lat) \n long:\(long)", lat: lat, long: long)
            })
            
            alert.addAction(circle)
            alert.addAction(polygon)
            
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    func setupCircleGeofencing(title: String, lat:Double ,long:Double, radius:Double,identifier: String ) {
        // remove all existing overlay
        
        // self.map.removeOverlays(self.map.overlays)
        // startMonitoring
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        locationManager.startMonitoring(for: region(with: location,radius: radius))
        
        // add annotation
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = title
        map.addAnnotation(annotation)
        
        // show overlay
        let circle = MKCircle(center: location,
                              radius: radius)
        map.addOverlay(circle)
    }
    
    func setupPolygonGeofencing(title: String, lat:Double ,long:Double) {
        
        polyCheck = true
        // remove all existing overlay
        
        //self.map.removeOverlays(self.map.overlays)
        // add annotation
        let annotation = MKPointAnnotation()
        annotation.coordinate = self.polygonCoordinate[0]
        annotation.title = title
        map.addAnnotation(annotation)
        
        
        // show overlay
        let polygon = MKPolygon(coordinates: self.polygonCoordinate, count: self.polygonCoordinate.count)
        map.addOverlay(polygon)
    }
    
    var polygonCoordinate = [CLLocationCoordinate2D]()
    
    func polygonAlert (withTitle title: String?, message: String?, lat : Double , long : Double) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        var titleStr : String!
        if polygonCoordinate.count == 0  {self.regionId = self.randomString(length: 10)} // new polygon
        if polygonCoordinate.count >= 3 { // must be 3 coords or more
            alert.addTextField(configurationHandler: { textField in
                textField.placeholder = "Input region title .."
            })
            
            alert.addTextField(configurationHandler: { textField in
                textField.placeholder = "Input Wifi Name  .."
            })
            
//            alert.addTextField(configurationHandler: { textField in
//                textField.placeholder = "Input Wifi Mac Address  .."
//            })
            titleStr = "Save and draw polygon shap"
        } else {
            titleStr = "Add this to polygon shap" 
            
        }
        alert.addAction(UIAlertAction(title: titleStr, style: .default, handler: { action in
            if self.polygonCoordinate.count >= 3 { // must be 2 coords or more
                self.polygonCoordinate.append(CLLocationCoordinate2D(latitude: lat, longitude: long))
                let coordinate = CoordinatesObject()
                coordinate.id = self.randomString(length: 10)
                coordinate.regionId = self.regionId
                coordinate.latitude = lat
                coordinate.longitude = long
                try! self.realm.write {
                    self.realm.add(coordinate)
                }
                let titleAlert = alert.textFields?[0].text
                // self.polygonCoordinate.removeAll()
                
                let regionObject = RegionObject()
                regionObject.id = self.regionId
                regionObject.title = titleAlert!
                regionObject.type = "polygon"
                regionObject.radius = 0.0 // polygon -> no need for radius
                
                try! self.realm.write {
                    self.realm.add(regionObject)
                }
                
                let ssidAlert = alert.textFields?[1].text
//                let bssidAlert = alert.textFields?[2].text
                
                let wifiObject = WifiObject()
                wifiObject.id = self.randomString(length: 10)
                wifiObject.regionId = self.regionId
                wifiObject.name = ssidAlert!
                wifiObject.macAddress = "macAddress"
                
                try! self.realm.write {
                    self.realm.add(wifiObject)
                }

                self.setupPolygonGeofencing(title: titleAlert!, lat: lat, long: long)
                self.regionId = "initial"
                self.polygonCoordinate = []
                self.polyCheck = false
                self.locate(self.map!)
            } else {
                if self.regionId == "initial" {
                    self.regionId = self.randomString(length: 10)
                }
                
                let coordinate = CoordinatesObject()
                coordinate.id = self.randomString(length: 10)
                coordinate.regionId = self.regionId
                coordinate.latitude = lat
                coordinate.longitude = long
                try! self.realm.write {
                    self.realm.add(coordinate)
                }
                self.polygonCoordinate.append(CLLocationCoordinate2D(latitude: lat, longitude: long))
                
            }
            
        }))
        
        self.present(alert, animated: true)
    }
    
    
    func region(with coordinate2D: CLLocationCoordinate2D ,radius: Double ) -> CLCircularRegion {
        let region = CLCircularRegion(center: coordinate2D, radius: radius, identifier: randomString(length: 10))
        region.notifyOnEntry = true
        region.notifyOnExit = true
        return region
    }
    
    @IBAction func locate(_ sender: Any) {
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
        map.zoom()
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    func hasWifi() -> Bool {
        var hasWifiB = false
        let wifis = self.realm.objects(WifiObject.self)
        if self.getWifiInfo().count > 0 {
            self.getWifiInfo().forEach { wifiInfor in
                if hasWifiB {
                    return
                }
                wifis.forEach { wifiObject in
                    if (wifiInfor.ssid == wifiObject.name) {
                        hasWifiB = true
                        return
                    } else {
                        hasWifiB = false
                    }
                }
            }
        }
        return hasWifiB
    }
    
    func hasWifi(_ regionId: String) -> Bool {
        var hasWifiB = false
        let wifis = self.realm.objects(WifiObject.self).filter("regionId = '\(regionId)'")
        if self.getWifiInfo().count > 0 {
            self.getWifiInfo().forEach { wifiInfor in
                if hasWifiB {
                    return
                }
                wifis.forEach { wifiObject in
                    if (wifiInfor.ssid == wifiObject.name) {
                        hasWifiB = true
                        return
                    } else {
                        hasWifiB = false
                    }
                }
            }
        }
        return hasWifiB
    }

    var circleCheck = false
    var polyCheck = false
}

var routIndex = 1

extension MainViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        map.showsUserLocation = status == .authorizedAlways
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        self.circleCheck = true
        //if self.polygonCoordinate.count == 0 { // means no polygon region on the same time
        self.title = "CHECK IN"
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        
        scheduleNotification(title: "CHECK IN", body: "You already enter the geofencing region ..! ")
        
        //}
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        self.circleCheck = false
        //  if self.polygonCoordinate.count == 0 { // means no polygon region on the same time
        
        if (!self.hasWifi()) { // means no specific wifi is connected
            
            self.title = "CHECK OUT"
            navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            scheduleNotification(title: "CHECK OUT", body: "You already exite the geofencing region ..! ")
        }
        //  }
        
    }
    
    func getDeviceCoordianates() -> CLLocationCoordinate2D? {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                return locationManager.location?.coordinate
        }
        return nil
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // zoom to currenuser location
        map.zoom()
        let regions = self.realm.objects(RegionObject.self)
        if regions.count == 0 {
            self.title = "Long-Press to the map to continue"
            Toast(text: "Long-Press to the map to continue").show()
            return
        }
        /////////////
        print("longitude \(Double((locations.last?.coordinate.longitude)!))\nlatitude \(Double((locations.last?.coordinate.latitude)!)) hasWifi : \(self.hasWifi())")
        // for circle
        let coordinates = self.realm.objects(CoordinatesObject.self)
        var isNoOneFound = true
        coordinates.forEach { coordinate in
            let regionObject = self.realm.object(ofType: RegionObject.self, forPrimaryKey: coordinate.regionId)
            if regionObject == nil || regionObject?.type == "polygon" {return}
            let point = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
            let region = self.region(with: point, radius: regionObject!.radius)
            
            if let currentCoordinates = getDeviceCoordianates() {
                if region.contains(currentCoordinates) {
                    self.title = "You are inside coordinate at \( regionObject?.title ?? "")"
                    Toast(text: "You are inside coordinate at  \(regionObject?.title ?? "")").show()
                    navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                    isNoOneFound = false
                } else if self.hasWifi(coordinate.regionId) {
                    self.title = "You are inside wifi at \( regionObject?.title ?? "")  "
                    Toast(text: "You are inside wifi at \( regionObject?.title ?? "") ").show()
                    navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                    isNoOneFound = false

                }
            }

        }
        if  isNoOneFound {
            self.title = "Monitering"
            navigationController?.navigationBar.barTintColor = .white
            Toast(text: "You are outside").show()
        }
//        // for polygon
//        let polygonRegions = self.realm.objects(RegionObject.self)
//        let pointCheck = CLLocationCoordinate2D(latitude: (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!)
//
//        polygonRegions.forEach { polygonRegion in
//            if polygonRegion.type == "circle" {return}
//            let path = GMSMutablePath()
//            let polygonCoordinates = self.realm.objects(CoordinatesObject.self).filter("regionId = '\(polygonRegion.id)'")
//
//            polygonCoordinates.forEach { polygonCoordinate in
//                let point = CLLocationCoordinate2D(latitude: polygonCoordinate.latitude, longitude: polygonCoordinate.longitude)
//                    path.add(point)
//            }
//            if (GMSGeometryContainsLocation(pointCheck, path, false)) {
//                self.title = "You are inside coordinate at  \( polygonRegion.title )"
//                Toast(text: "You are inside coordinate at \(polygonRegion.title )").show()
//                navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
//            }
//            
//            if (self.hasWifi(polygonRegion.id)) {
//                self.title = "You are inside wifi at \( polygonRegion.title )  "
//                Toast(text: "You are inside wifi at \(polygonRegion.title )  ").show()
//                navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
//            }
//        }
    }
}

extension MainViewController:UIGestureRecognizerDelegate {
}
extension MainViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView,
                 rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let circleOverlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: circleOverlay)
            circleRenderer.fillColor = .red
            circleRenderer.alpha = 0.5
            
            return circleRenderer
        }
        if let polygonOverlay = overlay as? MKPolygon {
            let polygonRenderer = MKPolygonRenderer(overlay: polygonOverlay)
            polygonRenderer.fillColor = .red
            polygonRenderer.alpha = 0.5
            
            return polygonRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
        
    }
}
