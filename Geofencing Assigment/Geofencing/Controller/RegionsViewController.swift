//
//  RegionsViewController.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/29/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//


import UIKit
import RealmSwift

class RegionsViewControllerCell:UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var type: UILabel!
    
}

class RegionsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private let cellId = "RegionsViewControllerCell"
    let realm = try! Realm()
    fileprivate var userRegions:  Results<RegionObject>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Regions"
        navigationController?.navigationBar.barTintColor = .white
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userRegions = realm.objects(RegionObject.self).sorted(byKeyPath: "created", ascending: false)
        userRegions.forEach({print($0.title)})
        self.tableView.reloadData()
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension RegionsViewController :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userRegions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RegionsViewControllerCell", for: indexPath) as! RegionsViewControllerCell        
        cell.title.text = userRegions[indexPath.row].title
        cell.type.text = userRegions[indexPath.row].type
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let regionDetailsViewController = R.storyboard.main.regionDetailsViewController() else {
            return
        }
        regionDetailsViewController.regionId = userRegions[indexPath.row].id
        self.navigationController?.pushViewController(regionDetailsViewController, animated: true)
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let region = userRegions[indexPath.row]
            let wifi = realm.objects(WifiObject.self).filter("regionId = '\(region.id)'").first
            let coordinates = realm.objects(CoordinatesObject.self).filter("regionId = '\(region.id)'").first

            try! realm.write {
                realm.delete(region)
                realm.delete(wifi!)
                realm.delete(coordinates!)

            }
            self.tableView.reloadData()
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
    
}
