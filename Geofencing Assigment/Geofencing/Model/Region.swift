//
//  Region.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/29/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//

import Foundation
import RealmSwift

class RegionObject: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var title = ""
    @objc dynamic var type = ""
    @objc dynamic var radius = 0.0
    @objc dynamic var created = Date()
    override class func primaryKey() -> String? {
        return "id"
    }
}

