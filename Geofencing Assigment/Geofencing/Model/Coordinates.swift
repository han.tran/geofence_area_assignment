//
//  Coordinates.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/29/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//

import Foundation
import RealmSwift
class CoordinatesObject: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var regionId = ""
    @objc dynamic var created = Date()
    override class func primaryKey() -> String? {
        return "id"
    }
}
