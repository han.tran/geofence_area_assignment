//
//  Wifi.swift
//  GeofencingAssigment
//
//  Created by Hana.Tran on 7/29/20.
//  Copyright © 2020 Hana.Tran. All rights reserved.
//

import Foundation
import RealmSwift

class WifiObject: Object {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var regionId = ""
    @objc dynamic var macAddress = ""
    override class func primaryKey() -> String? {
        return "id"
    }
}
