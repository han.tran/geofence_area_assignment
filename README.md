
# geofence_area_assignment

The goal of this assignment is to create an iOS application that will detect if the device is located inside of a geofence area.

Geofence area is defined as a combination of some geographic point, radius, and specific Wifi network name. A device is considered to be inside of the geofence area if the device is connected to the specified WiFi network or remains geographically inside the defined circle.

Note that if device coordinates are reported outside of the zone, but the device still connected to the specific Wifi network, then the device is treated as being inside the geofence area.

Application activity should provide controls to configure the geofence area and display current status: inside OR outside.

# How to build
- pod install
- build and run on real devices

# How to use
Setup circle shap and Wifi
- Long Press to google map and choose circle

Setup polygons shap and Wifi
to draw polygons shap need to setup more than 3 point (lat and lon)
So:
1 Long Press to google map and choose polygon
2 Repeat step 1 
